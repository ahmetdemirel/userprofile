from django.db import models
from django.contrib.auth.models import User






gender = (
    ( 'Male','M'),
    ( 'Female','F'),
)

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    gender = models.CharField(max_length=120, choices=gender, default='M', null=True)
    slug = models.SlugField(unique=True, editable=False)
    image = models.FileField(upload_to='media/', blank=True)
