# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='image',
            field=models.FileField(default=datetime.datetime(2017, 8, 8, 9, 8, 52, 201276, tzinfo=utc), upload_to='media/'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='gender',
            field=models.CharField(choices=[('Male', 'M'), ('Female', 'F')], default='M', max_length=120, null=True),
        ),
    ]
