from django.apps import AppConfig


class UserAppConfig(AppConfig):
    name = 'user_profile_app'
