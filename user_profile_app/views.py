from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login
from django.utils.text import slugify
from django.contrib.auth.models import User
from .forms import UserProfileForm, UserProfileForm2, UserLoginForm
from .models import UserProfile

def home_view(request):
    context = {}
    #return redirect('user_profile' )

    return render(request, 'home.html' )


def user_profile(request):

    context = {}
    context['form'] = UserProfileForm(request.POST or None)
    context['form2'] = UserProfileForm2(request.POST or None)
    if request.method == "POST":
        form = UserProfileForm(request.POST, request.FILES or None)
        form2 = UserProfileForm2(request.POST)


        if form.is_valid() and form2.is_valid():
            if form.cleaned_data and form2.cleaned_data:
                instance2 = User.objects.create_user(form2)
                #instance2 = form2.save(commit=False)
                instance2.username = form2.cleaned_data.get('username')
                instance2.first_name = form2.cleaned_data.get('first_name')
                instance2.last_name = form2.cleaned_data.get('last_name')
                instance2.email = form2.cleaned_data.get('email')
                instance2.password = form2.cleaned_data.get('password')



                instance = form.save(commit=False)
                instance.user = instance2
                instance.image = request.FILES['image']




                instance.gender = form.cleaned_data.get('gender')
                instance.slug = slugify(instance2.first_name+'-'+instance2.last_name)

                instance2.save()
                instance.save()
                #print (instance.user.first_name)


                return redirect('user_profile_page', slug=instance.slug)

    return render(request, 'forms.html', context)


def user_profile_page(request,slug):
    print(slug)
    #UserProfile.user=User.objects.get()
    instance = get_object_or_404(UserProfile, slug=slug)

    return render(request,'profile.html', {'user': instance})


def user_profile_edit(request, slug):
    user = get_object_or_404(UserProfile, slug=slug)
    form = UserProfileForm(instance=user)
    form2 = UserProfileForm2(instance=user.user)


    if request.method == "POST":
        form = UserProfileForm(request.POST, instance=user)
        form2 = UserProfileForm2(request.POST, instance=user)
        if form.is_valid() and form2.is_valid():
            if form.cleaned_data and form2.cleaned_data:
                user.user.first_name = form2.cleaned_data.get('first_name')
                user.user.last_name = form2.cleaned_data.get('last_name')
                user.user.email = form2.cleaned_data.get('email')
                user.gender = form.cleaned_data.get('gender')
                user.save()
                user.user.save()

                return redirect('user_profile_page', slug=user.slug)

    return render(request, 'profile_edit.html', {'form':form, 'form2':form2})


def user_login(request):
    title = "Login"
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        print (user)

    return render(request, "login.html", {"form":form, "title": title})

