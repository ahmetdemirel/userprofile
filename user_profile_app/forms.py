from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.forms import ModelForm
from django import forms
from .models import UserProfile


class UserProfileForm2(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'password',
            'email',

        ]

class UserLoginForm((forms.Form)):

    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class UserProfileForm(ModelForm):

    class Meta:
        model = UserProfile

        fields = [
            'gender',
            'image',
        ]


    def clean_first_name(self, *args, **kwargs):
        first_name = self.cleaned_data.get('first_name')
        if len(first_name) < 2:
            raise forms.ValidationError("Lutfen gecerli irst name giriniz")
        return first_name

    def clean_last_name(self, *args, **kwargs):
        last_name = self.cleaned_data.get('last_name')
        if len(last_name) < 2:
            raise forms.ValidationError("Lutfen gecerli last name giriniz")
        return last_name


