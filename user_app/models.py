from django.db import models
from django.utils.text import slugify

import random
import string





GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )


class userProfile(models.Model):
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    email = models.EmailField()
    slug = models.SlugField(unique=True, editable=False)
    slug2 = models.SlugField(unique=True, editable=False, default=''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8)))
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default='M', null=True)



    def get_unique_slug(self): # daha once olusmus slug var ise random 8 karakterli yeni slug olusturma islemei
        slug = slugify(self.first_name)
        unique_slug = slug
        while userProfile.objects.filter(slug=unique_slug).exists():
            unique_slug = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))

        return unique_slug




    def save(self, *args, **kwargs):  # View'deki save islemine buradan gonderme yapiyoruz
        if not self.slug:
            self.slug = self.get_unique_slug()


        return super(userProfile, self).save(*args, **kwargs)





