from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from .forms import UserProfileForm
from django.contrib import messages
from django.utils.text import slugify

from django.http import JsonResponse


def userProfile(request):

    context={}
    context['form'] = UserProfileForm(request.POST or None)


    if request.method == "POST":
        form = UserProfileForm(request.POST or None)

        if form.is_valid():
            instance=form.save(commit=False)
            instance.save()
        print(form.cleaned_data)

    return render(request, 'forms.html', context)


def validate_username(request):
    username = request.GET.get('first_name', None)
    data = {
        'is_taken': User.objects.filter(username__iexact=username).exists()
    }
    return JsonResponse(data)