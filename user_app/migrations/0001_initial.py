# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='userProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('first_name', models.CharField(max_length=120)),
                ('last_name', models.CharField(max_length=120)),
                ('email', models.EmailField(max_length=254)),
                ('slug', models.SlugField(unique=True, editable=False)),
                ('slug2', models.SlugField(unique=True, default='V0uMqJaH', editable=False)),
                ('gender', models.CharField(choices=[('M', 'Male'), ('F', 'Female')], default='M', null=True, max_length=1)),
            ],
        ),
    ]
